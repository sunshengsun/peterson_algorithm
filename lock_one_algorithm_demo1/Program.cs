﻿using System;
using System.Threading;

// lockone 算法  -- 简单软互斥锁实现
/*
 * 限于两个进程间的互斥
 *
 * 实现：
 * 使用一个 两个元素的bool数组，
 * 1。当某一个进程需要访问共享资源的时候，需要先将其中拥有的数组元素设置为true
 * 2。标记自己想要访问共享资源，也是告诉其他的进程，我要访问了，或者是我已经抢占了，你先忍一下
 * 3。然后while循环检查另一标记是否为true，如果为true，说明已经有进程在访问了，等等，等另一个进程将其标记设置为false，即表明
 *    它访问结束，允许别的进程访问了
 *
 * 有个缺点， 如果两个进程，同时拿到了共享资源，那就死锁了，互相持有对方等待的条件又不释放
 * 
 */
namespace lock_one_algorithm_demo1
{
    class Program
    {
        // 
        private static bool[] lockone = new bool[2] { false, false };
        private static int critical_var = 10;

        static void Tfunc(object thread_one)
        {
            int num = (int) thread_one;

            if (num < 0 || num > 1)
            {   
                Console.WriteLine("passed args error, only 0 or 1");
                return;
            }

            while (true)
            {
                // 标记自己需要访问共享资源了, 如果已经抢得了先访问共享资源的权限，那么这个标记也是告诉别的进程，我已经在访问共享资源了，你等等，忍一下
                lockone[num] = true;
                // 循环检查共享资源是否有其他进程正在占用着
                while (lockone[(num + 1) % 2])
                {
                    // 如果有别的进程在占用共享资源，则空转并持续尝试
                    ;
                }

                // 访问临界qu
                critical_var--;
                Console.WriteLine("{0} - {1}", Thread.CurrentThread.ManagedThreadId, critical_var);
                if (critical_var == 0)
                {
                    lockone[num] = false;
                    break;
                }
                // 访问共享资源结束，置标记为false
                lockone[num] = false;
            }
        }

        class args1
        {
            public int i;

            public args1(int j)
            {
                i = j;
            }
        }

        static void bfunc()
        {
            while ((lockone[0] && lockone[1]) || (!lockone[0] && !lockone[1]))
            {
                if (lockone[0])
                    lockone[0] = false;
                if (!lockone[0])
                {
                    lockone[0] = true;
                }
            }
        }

        static void Main(string[] args)
        {
            args1 a = new args1(0);
            args1 b = new args1(1);
            Thread t1 = new Thread(new ParameterizedThreadStart(Tfunc));
            Thread t2 = new Thread(new ParameterizedThreadStart(Tfunc));
            Thread t3 = new Thread(new ThreadStart(bfunc));
            t3.Start();
             
            t2.Start(a.i);
            t1.Start(b.i);

            t1.Join();
            t2.Join();
            t3.Join();
            
            Console.WriteLine("Hello World!");
        }
    }
}