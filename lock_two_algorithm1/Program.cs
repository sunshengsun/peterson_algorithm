﻿using System;
using System.Threading;


/*
 * locktwo算法是一种谦让算法， 用于实现两个进程 的 对共享变量的 互斥操作
 *
 * 使用一个int 型变量turn， 如果该变量的值 等于 变量 中设定的或者说是 期待的，那自己就等待， 知道turn是自己所不期待的，就进入临界区执行
 *
 * 假设： 两个进程a， b， a对turn的期待是0， b对turn的期待是1，每当他们在进入临界区之间，就会循环检查这个值，如果这个值是自己期待的，就等待
 * 让出进入临界区的权利，每当他们出临界区，就改变这个值，以让另一个进程能运行
 *
 * 因为对int操作是原子的，且turn用volatile修饰，因为这test-and-set，每个核心对turn的修改一定是从主存中获取到的最新值，
   当线程a运行时，对turn赋值了它期待的值，这时，线程a是谦让的，线程b同时运行，且也对turn进行了修改，修改成了自身的期待的值
   在两个的while里面，
 * 
 */
namespace lock_two_algorithm1
{
    class Program
    {
        private static volatile int turn;

        private static int share_max = 10;
        
        // 线程方法
        static void func1(object num)
        {
            while (true)
            {
                
                int i = (int) num;
                // 对turn 赋值, 如果同时拿到了对turn的修改，另一个进程能立即见到修改
                turn = i;
                // 循环判断是否有别的进程在临界区里面
                while (turn == i) // 前面已经先赋值了，肯定是相等，也肯定会等待，所以是谦让式的
                {
                    ; // 空转，不停的尝试判断，如果有一次不是相等的，就意味着另一个进程主动让出或者已经退出了临界区，可以进入临界区了
                }

                // 临界区
                // share_max--;
                Console.WriteLine(share_max--);

                // 修正turn为自己期待的，作出谦让
                turn = (int) num;
            }
        }
        
        static void Main(string[] args)
        {
            Thread[] ta = new Thread[2];
            for (int i = 0; i < 2; i++)
            {
                ta[i] = new Thread(new ParameterizedThreadStart(func1));
            }
            
            ta[0].Start(0);
            ta[1].Start(1);

            ta[0].Join();
            ta[1].Join();
            
            Console.WriteLine("Hello World!");
        }
    }
}