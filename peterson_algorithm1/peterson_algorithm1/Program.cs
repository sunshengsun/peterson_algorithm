﻿using System;
using System.Threading;

/* peterson 双线程 软锁算法
 * 使用flag、turn双变量控制
 *
 * 有两中实现方式
 * 1 谦让式
 * 2 主动式
 * 
 */
namespace peterson_algorithm1
{
    class Program
    {
        // 想进入临界区的进程就设置 自己的flag【self】 为true
        private static bool[] flag = new Boolean[2] {false, false};
        private static int turn; // 控制哪个进程能进入临界区, 当为i的时候，表示i进程可以进入，当为j时，表示j进程可以进入
        private static int share_var = 100;
        
        static void mylock(object tid)
        {
            int i = (int) tid;

            flag[i] = true;
            turn = 1-i; // 主动让出进入临界区的机会
            // 这里 有两个 条件共同作用是 等待 还是进入临界区， 谦让式的做法 是 等待 别的进程谦让
            while (flag[1 - i] && turn == 1 - i)
            {
                ;
            }
        }

        static void myunlock(int tid)
        {
            flag[tid] = false;
        }

        static void Pt1(int tid)
        {
            Console.WriteLine("{0} running", tid.ToString());
            Console.WriteLine("share_var == {0}", (--share_var).ToString());
        }

        static void tfunc(object tid)
        {
            while (true)
            {
                mylock(tid);
                if (share_var == 0)
                    break;
                Pt1((int) tid);
                myunlock((int) tid);
            }
            myunlock((int)tid); 
            Console.WriteLine("{0} exited", (int)tid);
        }
        
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ParameterizedThreadStart(tfunc));
            Thread t2 = new Thread(new ParameterizedThreadStart(tfunc));
            
            t1.Start(0);
            t2.Start(1);

            t1.Join();
            t2.Join();
            
            Console.WriteLine("Hello World!");
        }
    }
}